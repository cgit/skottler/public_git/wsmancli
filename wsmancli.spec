Name:           wsmancli
BuildRequires:  gcc-c++ libwsman-devel >= 2.2.7 libwsman_clientpp-devel >= 2.2.7 pkgconfig
%if 0%{?suse_version} > 1010
BuildRequires:  libcurl-devel
%else
BuildRequires:  curl-devel
%endif
Version:        2.3.4
Release:        1
Url:            http://www.openwsman.org/
License:        BSD3c
Group:          System/Management
Summary:        Opensource Implementation of WS-Management - Command line utility
Source:         %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
Part of Openwsman, an Opensource Implementation of WS-Management

%prep
%setup -q

%build
%configure
make %{?jobs:-j%jobs}

%install
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/wsman
%{_bindir}/wseventmgr

%changelog
* Sun Jun 30 2013 Sam Kottler <shk@redhat.com> 2.3.4-1
- Some improvements to the spec (shk@redhat.com)

* Sun Jun 30 2013 Sam Kottler <shk@redhat.com> 2.3.3-1
- new package built with tito
